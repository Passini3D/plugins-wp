<?php wp_footer(); ?>
<section class="row social alinha-centro">
  <div class="container alinha-centro">
    <ul class="social-itens alinha-centro-horizontal">
      <?php
        if( have_rows('redes_sociais', 'option') ):
            while ( have_rows('redes_sociais', 'option') ) : the_row();
      ?>
        <li><a href="<?php the_sub_field('link_social'); ?>" target="_blank"><i class="fab fa-<?php the_sub_field('rede_social'); ?>"></i></a></li>
      <?php
            endwhile;
        endif;
      ?>
    </ul>
  </div>
</section>
<footer class="page-footer">
  <div class="container">
    <div class="row">
      
      <div class="col l9 s12 map-site">
        <h5><a href="#">Links</a></h5>
        <ul class="submenu-footer">
          <li><a href="#!">Link 1</a></li>
          <li><a href="#!">Link 2</a></li>
          <li><a href="#!">Link 3</a></li>
          <li><a href="#!">Link 4</a></li>
        </ul>
      </div>
      <div class="col l3 s12">
        <img class="responsive-image" src="<?php the_field('logo_rodape', 'option'); ?>" alt="">
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
    Copyright © 2018 - Ânima Educação. Todos os direitos reservados.
    </div>
  </div>
</footer>

<!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="<?=get_template_directory_uri(); ?>/slick/slick.js"></script>
  <script src="<?=get_template_directory_uri(); ?>/js/materialize.js"></script>
  <script src="<?=get_template_directory_uri(); ?>/js/init.js"></script>

  </body>
</html>