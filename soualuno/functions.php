<?php
add_theme_support('post-thumbnails');
add_action('init', 'registrarMenu');

function registrarMenu(){
	register_nav_menu('header-menu', 'main-menu');
	register_nav_menu('menu-topo', 'top-menu');	
}

function geraTitle(){
	bloginfo('name');
	if (!is_home()) echo " | ";
	the_title();
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Opções Gerais',
		'menu_title'	=> 'Opções Gerais',
		'menu_slug' 	=> 'opcoes-gerais',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Banner Home',
		'menu_title'	=> 'Banner Home',
		'menu_slug' 	=> 'banner-home',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}