<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title><?php geraTitle(); ?></title>

  <!-- HEAD WP-->
  <?php wp_head(); ?>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto" rel="stylesheet">
  <link href="<?=get_template_directory_uri(); ?>/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?=get_template_directory_uri(); ?>/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?=get_template_directory_uri(); ?>/slick/slick.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?=get_template_directory_uri(); ?>/slick/slick-theme.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
<?php include(locate_template('menus.php')); ?>