<?php get_header(); ?>

<style type="text/css">
	<?php

		if( have_rows('banner_home', 'option') ):
			$contb = 1;
		    while ( have_rows('banner_home', 'option') ) : the_row();
	?>
		.banner-<?= $contb; ?>{
			background:url('<?php the_sub_field("banner_desktop"); ?>') 50% no-repeat; 
		}
	<?php
		$contb++;
		    endwhile;
		endif;

	?>
	
	@media (max-width:768px){
		<?php

			if( have_rows('banner_home', 'option') ):
				$contb = 1;
			    while ( have_rows('banner_home', 'option') ) : the_row();
		?>
			.banner-<?= $contb; ?>{
				background:url('<?php the_sub_field("banner_mobile"); ?>') 50% no-repeat;
			}
		<?php
			$contb++;
			    endwhile;
			endif;

		?>
	}
	.ch-img-1 { 
		background-image: url(http://localhost:8888/ett/wp-content/uploads/2018/06/livros.jpg);
		-webkit-box-shadow: inset 200px 200px #fff;
            box-shadow: inset 200px 200px #fff;
	} 

</style>

<div class="banner-home">
    <?php
		if( have_rows('banner_home', 'option') ):
			$contb = 1;
		    while ( have_rows('banner_home', 'option') ) : the_row();
	?>
			<div class="banner banner-<?= $contb; ?> alinha-centro-vertical">
				<div class="container">
					<div class="box-banner">
						<h2 style="color: <?php the_sub_field('cor_titulo'); ?>"><?php the_sub_field('titulo_banner'); ?></h2>
						<div style="color: <?php the_sub_field('cor_descricao'); ?> !important;">
							<?php the_sub_field('descricao_banner'); ?>
						</div>
						<?php if(get_sub_field('ativar_btn')): ?>
							<a class="btn-large pink-btn waves-effect" href="<?php the_sub_field('link_btn'); ?>"><?php the_sub_field('label_btn'); ?></a>
						<?php endif; ?>
					</div>
				</div>
			</div>
	<?php
		$contb++;
		    endwhile;
		endif;
	?>
</div>

<section class="row sobre">
	<article class="container alinha-centro-vertical">
		<div class="col s12 m6">
			<h2>O que é o</h2>
			<h1>Sou Aluno?</h1>
		</div>
		<div class="col s12 m6">
			<h4><?php the_field('titulo_sobre'); ?></h4>
			<?php the_field('sobre_soualuno'); ?>
		</div>
	</article>
</section>

<section class="row servicos">
	<article class="container">
		<center><h3>Serviços a comunidade</h3></center>
		<div class="servicos-lista">
			<!--Loop deste item do post type "servicos-comunidade"-->
			<div class="servico-item">
				<a href="#"><!--permalink do post-->
					<center>
						<div class="servico-img">
							<img src="http://localhost:8888/soualuno-sample2/wp-content/uploads/2018/07/img-test.jpg"><!--peguar feature image do post-->
						</div>
					</center>
					<div class="servico-info">
						<h6>Titulo</h6>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</p><!--Exibir os primeiros 124 caracteres do the_content do post-->
					</div>
				</a>
			</div>
			<!--Loop deste item do post type "servicos-comunidade"-->
		</div>
	</article>
</section>

<section class="row acessos-rapidos">
	<article class="container">
		<h3 class="purple-title">Acessos rápidos:</h3>

		<div class="acessos-lista alinha-topo-horizontal">
			<?php
		        if( have_rows('acessos_home') ):
		            while ( have_rows('acessos_home') ) : the_row();
		      ?>
		        <div class="col s12 m6 l4">
		        	<div class="acesso-item">
		        		<div class="row acesso-header">
							<h4><?php the_sub_field('nome_acesso'); ?></h4>
						</div>
						<div class="row acesso-body">
							<div class="acesso-img">
								<center><img class="responsive-img" src="<?php the_sub_field('imagem_destaque'); ?>"></center>
							</div>
							<div class="acesso-info">
								<h6><?php the_sub_field('titulo_acesso'); ?></h6>
								<?php the_sub_field('sobre_acesso'); ?>
								<a href="<?php the_sub_field('link_acesso'); ?>" target="_blank" class="btn purple-btn right">Saiba mais</a>
							</div>
						</div>
		        	</div>
				</div>
		      <?php
		            endwhile;
		        endif;
		      ?>
		</div>
	</article>
</section>

<section class="row unidades">
	<article class="container">
		<h3 class="purple-title">Conheça nossas unidades:</h3>
		<div class="unidades-lista">
			<!--Loop deste item do post type "unidades"-->
			<div class="unidade-item alinha-centro-horizontal">
				<div class="grid">
					<figure class="effect-oscar">
						<img class="responsive-img" src="http://localhost:8888/soualuno-sample2/wp-content/uploads/2018/07/img_unidade.jpg"/>
						<figcaption>
							<h2><span class="txt-unidade">Unidade</span> <span>Mooca</span></h2>
						</figcaption>
					</figure>
				</div>
			</div>
			<!--Loop deste item do post type "unidades"-->
		</div>
	</article>
</section>
<?php get_footer(); ?>


  
