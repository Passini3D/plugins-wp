(function($){
  $(function(){

    $('.sidenav').sidenav();

    $('select').formSelect();

    $('.mask img').addClass("materialboxed");

    $('.materialboxed').materialbox();

    $('.modal').modal();

    $('.collapsible').collapsible();

    $('.banner-home').slick({
	  dots: true,
	  infinite: true,
	  adaptiveHeight: true
	});

	$('.servicos-lista').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  dots: false,
	  adaptiveHeight: true,
	  responsive: [
	    {
	      breakpoint: 1700,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        adaptiveHeight: true,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 1056,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        adaptiveHeight: true,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 700,
	      settings: {
	        slidesToShow: 1,
	        adaptiveHeight: true,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

	$('.unidades-lista').slick({
	  infinite: true,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  dots: false,
	  adaptiveHeight: true,
	  responsive: [
	    {
	      breakpoint: 1700,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        adaptiveHeight: true,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 1056,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        adaptiveHeight: true,
	        infinite: true,
	        dots: false
	      }
	    },
	    {
	      breakpoint: 700,
	      settings: {
	        slidesToShow: 1,
	        adaptiveHeight: true,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

    $('input.autocomplete').autocomplete({
      data: {
        "Apple": null,
        "Microsoft": null,
        "Google": 'https://placehold.it/250x250'
      },
    });

    $(window).scroll(function(){
		if($(window).scrollTop() == 0) {
			$('#menu-fixo').removeClass('navbar-fixed');
			$('.bk-menu').removeClass('to-top');
	    } else {
	        $('#menu-fixo').addClass('navbar-fixed');
	        $('.bk-menu').addClass('to-top');
	    }
		
	});

    $('.search').click(function(){
    	$('.searchbar').toggle();
    });
    
    $( ".collapsible-header" ).click(function() {
 		$(this).toggleClass('pink-accordeon');
	});

    $(".menu-item-has-children").children('a').addClass("dropdown");
    $(".dropdown").addClass("alinha-centro-vertical");
    $(".dropdown").append('<i class="material-icons right">arrow_drop_down</i>');

	$(".menu-item-has-children").click(function(){
		$( this ).children('.sub-menu').toggle();
	});
	


  }); // end of document ready
})(jQuery); // end of jQuery name space
