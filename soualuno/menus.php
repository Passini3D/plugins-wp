<nav class="top-menu hide-on-med-and-down" role="navigation">
  <div class="container alinha-centro-vertical separa-cantos">

    <div class="accessibility-bar alinha-centro-vertical">
      <span>Acessibilidade</span>
      <a href="#" class="font-control increase-size">A</a>
      <a href="#" class="font-control decrease-size">A</a>
      <a href="#" class="high-contrast increase-size"><i class="material-icons">iso</i></a>
      <a href="http://www.vlibras.gov.br/" target="_blank"><img width="25px" src="http://localhost:8888/soualuno-sample2/wp-content/uploads/2018/07/sem_assinatura_64x64.png"></a>
    </div>
      
    <!-- INI MENU -->
    <div class="hide-on-med-and-down">
      <?php 
        wp_nav_menu(array('theme_location' => 'menu-topo') );
      ?>
    </div>
    <!-- FIM MENU --> 
  </div>
    
  </div>
</nav>

<div class="" id="menu-fixo">
  <nav class="bk-menu" role="navigation">
    <?php
      $url = home_url();
    ?>
    <div class="nav-wrapper container"><a id="logo-container" href="<?php echo esc_url( $url ); ?>" class="brand-logo"><div class="logo" style="background:url(<?php the_field('logo_menu', 'option'); ?>) 50% no-repeat; background-size: contain;"></div></a>

      
      <ul class="right hide-on-med-and-down">
        <li><a class="dropdown-trigger alinha-centro-vertical search" href="#!" data-target="searchbar"><i class="material-icons">search</i></a></li>
      </ul>
      <!-- INI MENU -->
      <div class="right hide-on-med-and-down">
        <?php 
  			 wp_nav_menu(array('theme_location' => 'header-menu') ); 
  			?>
      </div>
      

      <div id="nav-mobile" class="sidenav">
        <?php 
  			 wp_nav_menu(array('theme_location' => 'header-menu') );
         wp_nav_menu(array('theme_location' => 'menu-topo') ); 
  			?>
      </div>
      <!-- FIM MENU --> 

      <a href="#" data-target="nav-mobile" class="sidenav-trigger flex-banner"><i class="material-icons">menu</i></a>
    </div>
  </nav>
  <nav class="row searchbar">
    <div class="nav-wrapper container">
      <form>
        <div class="input-field">
          <input id="search" type="search" required>
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav>
</div>