<div class="row header-pages alinha-centro-vertical" style="background: url('<?php the_field('header_img'); ?>') 50% no-repeat;">
  <div class="container">
    <nav class="breadcrumb-header">
      <div class="nav-wrapper">
        <div class="col s12">
          <a href="#!" class="breadcrumb"><i class="material-icons">home</i> First</a>
          <a href="#!" class="breadcrumb">Second</a>
          <a href="#!" class="breadcrumb">Third</a>
        </div>
      </div>
    </nav>
  </div>
	<div class="container alinha-self-topo">
		<center><h1 class="purple-title"><?php the_title(); ?></h1></center>
		<center><h4><?php the_field('subtitulo_header'); ?></h4></center>
	</div>
</div>