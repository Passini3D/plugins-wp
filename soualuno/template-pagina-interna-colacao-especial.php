<?php
/*
 * Template Name: Página interna colação especial
*/
?>
<?php get_header(); ?>
<?php include(locate_template('partial-header-pages.php')); ?>

<section class="row">
	<article class="container">
		<div class="col s12">
			<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				}
			?>
		</div>
	</article>
</section>

<section class="row">
	<article class="container">
    <ul class="collapsible">
	    <?php if(have_rows('faq')): ?>
		    <?php while(have_rows('faq')) : the_row(); ?>
		        <li>
		          <div class="collapsible-header alinha-pontas-horizontal"><span class="purple-title"><?php the_sub_field('pergunta'); ?></span> <i class="material-icons right">add</i></div>
		          <div class="collapsible-body"><span><?php the_sub_field('resposta'); ?></span></div>
		        </li>
		    <?php endwhile; ?>
	    <?php endif; ?>
    </ul>
    <?php if(have_rows('faq')): ?>
      <h4 class="purple-title">Serviços oferecidos</h4>
    <?php endif; ?>
	</article>
</section>

<?php get_footer(); ?>