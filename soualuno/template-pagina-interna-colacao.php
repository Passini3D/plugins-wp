<?php
/*
 * Template Name: Página interna colação
*/
?>
<?php get_header(); ?>
<?php include(locate_template('partial-header-pages.php')); ?>

<section class="row">
	<article class="container">
		<div class="col s12">
			<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				}
			?>
		</div>
	</article>
</section>

<section class="row">
	<article class="container">
      <h4 class="purple-title">Escolha o seu tipo de colação</h4>
	    <?php if(have_rows('tipos_colacoes')): ?>
		    <?php while (have_rows('tipos_colacoes')) : the_row(); ?>
		        <div class="col s12 m6 l4">
		        	<a href="<?php the_sub_field('link_colacao'); ?>">
			        	<div class="item-tipo-col alinha-centro">
			        		<center><h5><?php the_sub_field('nome_colacao'); ?></h5></center>
			        	</div>
		        	</a>
		        </div>
		    <?php endwhile; ?>
	    <?php endif; ?>
	</article>
</section>


<?php get_footer(); ?>