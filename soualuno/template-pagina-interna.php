<?php
/*
 * Template Name: Página interna
*/
?>
<?php get_header(); ?>
<?php include(locate_template('partial-header-pages.php')); ?>

<section class="row">
	<article class="container">
		<div class="col s12 m8">
			<?php 
				if ( have_posts() ) {
					while ( have_posts() ) {
						the_post();
						the_content();
					}
				}
			?>
		</div>
		<div class="col s12 m4">
			<?php if(get_field('telefone_contato')): ?>
				<div class="col s12 page-info">
					<center><i class="material-icons">phone_in_talk</i></center>
					<h5 class="purple-title">Telefone para contato</h5>
					<h6><?php the_field('telefone_contato'); ?></h6>
				</div>
			<?php endif; ?>
			<?php if(have_rows('horario_funcionamento')): ?>
		    	<div class="col s12 page-info">
		    		<center><i class="material-icons">access_alarm</i></center>
					<h5 class="purple-title">Horário de atendimento</h5>
		    <?php while ( have_rows('horario_funcionamento') ) : the_row(); ?>
		        <p><strong><?php the_sub_field('dias_atendimento'); ?>:</strong> <?php the_sub_field('horarios_atendimento'); ?></p>
		    <?php endwhile; ?>
		    	</div>
		    <?php endif; ?>
		</div>
	</article>
</section>

<section class="row">
	<article class="container">
    <?php if(have_rows('servicos_oferecidos')): ?>
      <h4 class="purple-title">Serviços oferecidos</h4>
    <?php endif; ?>

    <?php if(have_rows('servicos_oferecidos')): ?>
      <h4 class="purple-title">Serviços oferecidos</h4>
      <ul class="collapsible">
    <?php while (have_rows('servicos_oferecidos')) : the_row(); ?>
        <li>
          <div class="collapsible-header alinha-pontas-horizontal"><span class="purple-title"><?php the_sub_field('servico'); ?></span> <i class="material-icons right">add</i></div>
          <div class="collapsible-body"><span><?php the_sub_field('descricao_servico'); ?></span></div>
        </li>
    <?php endwhile; ?>
      </ul>
    <?php endif; ?>
	</article>
</section>

<?php if(get_field('fotos')): ?>
<section class="row purple-bg">
  <article class="container">
    <center><h4 class="white-text">Galeria de fotos</h4></center>
    <div class="galeira-pagina">
      <?php 

        $images = get_field('fotos');

        if( $images ): ?>
            <?php foreach( $images as $image ): ?>
                <img class="materialboxed col s12 m4" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endforeach; ?>
        <?php endif; ?>

    </div>
  </article>
</section>
<?php endif; ?>

<?php if(get_field('mapa')): ?>
<section class="row">
  <article class="container">
    <center><h4 class="titulo-cinza">Localização</h4></center>
  </article>
  <div class="mapa">
      <?php the_field('mapa'); ?>
    </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>